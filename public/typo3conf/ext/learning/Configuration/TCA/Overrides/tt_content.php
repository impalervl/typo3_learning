<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'learning',
    'Jumborton',
    'Jumborton',
    'EXT:learning/Resources/Public/Icons/Extension.svg'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'jumborton',
        'jumborton',
        'EXT:learning/Resources/Public/Icons/Extension.svg'
    ),
    'special',
    'learning'
);

$fields = array(
    'j_header' => [
        'label' => 'Header',
        'description' => 'It is Jumborton Header',
        'config' => [
            'type' => 'input',
            'max' => 150,
            'required' => true
        ],
    ],
    'j_sub_header' => [
        'label' => 'Subheader',
        'config' => [
            'type' => 'input',
            'max' => 150,
        ],
    ],
    'j_text' => [
        'label' => 'Body',
        'config' => [
            'type' => 'text',
            'enableRichtext' => true,
        ],
    ],
    'j_button_url' => [
        'label' => 'Button URL',
        'config' => [
            'type' => 'input',
            'renderType' => 'inputLink',
        ],
    ],
    'j_button_text' => [
        'label' => 'Button Text',
        'config' => [
            'type' => 'input',
            'max' => 30
        ],
    ]

);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $fields);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_content',
    'jumborton',
    'j_header,j_sub_header,j_text,j_button_url,j_button_text',
    null
);

$GLOBALS['TCA']['tt_content']['types']['jumborton'] = array(
    'showitem' => '--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
                   --linebreak--,j_header,--linebreak--,j_sub_header,--linebreak--,j_text,j_button_url,--linebreak--,j_button_text'
);

