mod.wizards.newContentElement.wizardItems.special {
    elements {
        jumborton {
            iconIdentifier = your-icon-identifier
            title = LLL:EXT:learning/Resources/Private/Language/locallang_db.xlf:title
            description = LLL:EXT:learning/Resources/Private/Language/locallang_db.xlf:description
            tt_content_defValues {
                CType = jumborton
            }
        }
    }
    show := addToList(jumborton)
}
