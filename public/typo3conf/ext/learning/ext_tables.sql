CREATE TABLE tt_content (
    j_header varchar(255) DEFAULT '',
    j_sub_header varchar(255) DEFAULT '',
    j_text text DEFAULT '',
    j_button_url varchar(255) DEFAULT '',
    j_button_text varchar(255) DEFAULT ''
);